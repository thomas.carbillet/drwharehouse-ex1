import pandas as pd
import sqlite3
import datetime

# -------------------
# Reading and filtering Excel File
# -------------------
df = pd.read_excel('files/organized/export_patient.xlsx', converters={'HOSPITAL_PATIENT_ID':str, 'DATE_MORT':str, 'DATE_NAISSANCE':str})
# Filtering by TEL and PRENOM seems to be good.
# Other filters could be implemented
df_duplicate = df[df.duplicated(subset=['TEL','PRENOM'], keep='first')]
# Here we keep the first duplicated row.
# One improvement would be to keep the row with the less NaN values
df = df.drop_duplicates(subset=['TEL','PRENOM'], keep='first')

# -------------------
# Adding rows to DB
# -------------------
# Connecting to DB
conn=sqlite3.connect("drwh.db")
cur = conn.cursor()

# Remove existing rows (for testing only) from tables DWH_PATIENT and DWH_PATIENT_IPPHIST
cur.execute("""DELETE FROM `DWH_PATIENT`;""")
cur.execute("""DELETE FROM `DWH_PATIENT_IPPHIST`;""")

# Creating and executing sql queries
for i, p in df.iterrows():
    # First we insert data into DWH_PATIENT
    birth_date = datetime.datetime.strptime(p['DATE_NAISSANCE'], "%d/%m/%Y").strftime("%Y-%m-%d")
    if str(p['DATE_MORT']) != 'nan':
        death_date = datetime.datetime.strptime(str(p['DATE_MORT']), "%d/%m/%Y").strftime("%Y-%m-%d")
        death_code = 1
    else:
        death_date = None
        death_code = 0
    cur.execute('INSERT INTO `DWH_PATIENT` (`LASTNAME`,`FIRSTNAME`,`BIRTH_DATE`,`SEX`,`MAIDEN_NAME`,`RESIDENCE_ADDRESS`,`PHONE_NUMBER`,`ZIP_CODE`,`RESIDENCE_CITY`,`RESIDENCE_COUNTRY`,`DEATH_DATE`,`DEATH_CODE`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)',
               (p['NOM'],p['PRENOM'],birth_date,p['SEXE'],p['NOM_JEUNE_FILLE'],p['ADRESSE'],p['TEL'],p['CP'],p['VILLE'],p['PAYS'],death_date,death_code))

    # Get last inserted id
    last_id = cur.lastrowid

    # Next we insert data into DWH_PATIENT_IPPHIST
    cur.execute('INSERT INTO `DWH_PATIENT_IPPHIST` (`PATIENT_NUM`,`HOSPITAL_PATIENT_ID`) VALUES (?,?)',
               (last_id,p['HOSPITAL_PATIENT_ID']))

# Adding duplicate IPP to DWH_PATIENT_IPPHIST
for i, p in df_duplicate.iterrows():
    cur.execute('SELECT `PATIENT_NUM` FROM `DWH_PATIENT` WHERE `PHONE_NUMBER` = ? AND `FIRSTNAME` = ?', (p['TEL'],p['PRENOM']))
    row = cur.fetchone()
    id_patient = row[0]
    cur.execute('INSERT INTO `DWH_PATIENT_IPPHIST` (`PATIENT_NUM`,`HOSPITAL_PATIENT_ID`) VALUES (?,?)',
               (id_patient,p['HOSPITAL_PATIENT_ID']))

# Commiting all the changes to DB
conn.commit()

# Closing DB
conn.close()
