import glob
import sqlite3
from utils.manage_files import ManageFiles

path = 'files/unorganized/'
filenames = glob.glob(path + '*')

# Connecting to DB
conn=sqlite3.connect("drwh.db")
cur = conn.cursor()
# Remove existing rows (for testing only) from tables DWH_DOCUMENT
cur.execute('DELETE FROM `DWH_DOCUMENT`;')

for f in filenames:

    # Open and extract text from files in files/unorganized folder
    manage_files = ManageFiles(f, path)
    text, doc_date, author = manage_files.extract_content()
    ipp = manage_files.get_ipp()
    document_num = manage_files.get_document_num()
    extension = manage_files.get_extension()

    cur.execute('SELECT `PATIENT_NUM` FROM `DWH_PATIENT_IPPHIST` WHERE `HOSPITAL_PATIENT_ID` = ?', [ipp])
    patient_num = cur.fetchone()[0]

    cur.execute('INSERT INTO `DWH_DOCUMENT`(`PATIENT_NUM`,`DISPLAYED_TEXT`,`DOCUMENT_TYPE`,`DOCUMENT_DATE`,`AUTHOR`,`ID_DOC_SOURCE`) VALUES(?,?,?,?,?,?)', (patient_num, text, extension, doc_date, author, document_num))

# Commiting all the changes to DB
conn.commit()

# Closing DB
conn.close()
