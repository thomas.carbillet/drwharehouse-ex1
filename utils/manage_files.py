import os.path
import pdftotext
import docx
import re
import datefinder

class ManageFiles:
    """
    ManageFiles gives the methods to open and extract content from pdf and docx files
    """
    def __init__(self, filename, path):
        """ Initializing ManageFiles
        Args:
            filename (string): one filename
            path (string): where the files are stored
        """
        self.filename = filename
        self.extension = os.path.splitext(filename)[1]
        self.path = path

    def extract_content(self):
        """ Extract content from files
        Returns:
            text (string): content ascii encoded
            document_date (date): written document date
            document_author (string): author of the document
        """
        if self.extension == '.pdf':
            text = self.__extract_pdf_content()
        elif self.extension == '.docx':
            text = self.__extract_docx_content()
        else:
            raise Exception('Only .docx and .pdf files are supported.')

        document_date = self.__get_document_date(text)
        # Encode string to ASCII
        text = text.encode()
        document_author = self.__get_document_author(text)

        return text, document_date, document_author

    def __extract_pdf_content(self):
        """ Extract raw content from pdf files
        Returns:
            content (string): raw content
        """
        content = ""
        with open(self.filename, "rb") as file:
            pdf = pdftotext.PDF(file)
        for page in pdf:
            content += page
        return content

    def __extract_docx_content(self):
        """ Extract raw content from docx files
        Returns:
            content (string): raw content
        """
        content = ''
        doc = docx.Document(self.filename)
        for p in doc.paragraphs:
            content += '\r' + p.text
        return content

    def get_ipp(self):
        """ Get Patient IPP
        Returns:
            ipp (string): patient ipp
        """
        ipp_re = re.search(self.path + '(.*)_', self.filename)
        return ipp_re.group(1)

    def get_extension(self):
        """ Get file extension
        Returns:
            extension (string): file extension
        """
        return self.extension

    def get_document_num(self):
        """ Get document number
        Returns:
            doc_num (string): document number
        """
        doc_num = re.search('_(.*)[.]', self.filename)
        return doc_num.group(1)


    def __get_document_date(self, content):
        """ (Private) Get document written date
        Args:
            content (string): content of the document
        Returns:
            date (date): written date of the document
        """
        dates = datefinder.find_dates(content)
        # We take the most recent date we found on the content
        # Different filters could be implemented for improvement
        date = max(dates).date()
        return date

    def __get_document_author(self, content):
        """ (Private) Get document author
        Args:
            content (string): content of the document
        Returns:
            author (string): author of the document
        """
        last_row = content.splitlines()[-1]
        author = (last_row.lstrip()).decode()
        reg_list = ['Dr ', 'Dr. ', 'DR']
        if not any(el in author for el in reg_list):
            author = None
        return author
